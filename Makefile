################################################################################
#   Copyright 2015 Big Data Partnership Ltd                                    #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
################################################################################

#########################################
# Makefile for building virtual containers

exit:
	echo "What do you want to build? "

build-docker-bdpcentos6:
	docker build --rm -t bdpzone/bdpcentos6:latest centos6/

build-docker-bdpcentos7:
	docker build --rm -t bdpzone/bdpcentos7:latest centos7/

build-docker-bdporaclelinux6:
	docker build --rm -t bdpzone/bdporaclelinux6:latest oraclelinux6/

build-docker-bdporaclelinux7:
	docker build --rm -t bdpzone/bdporaclelinux7:latest oraclelinux7/

build-docker-bdpfedora21:
	docker build --rm -t bdpzone/bdpfedora21:latest fedora21/

build-docker-openvpn:
	docker build --rm -t bdpzone/bdpopenvpn:latest openvpn

start-container-bdpcentos6:
	ansible-playbook -i hosts container/playbook.yml --extra-vars="@vars.yml" -e container_start_number=1 -e container_end_number=9 -e type=centos6

start-container-bdpcentos7:
	ansible-playbook -i hosts container/playbook.yml --extra-vars="@vars.yml" -e container_start_number=11 -e container_end_number=19 -e type=centos7

start-container-bdporaclelinux6:
	ansible-playbook -i hosts container/playbook.yml --extra-vars="@vars.yml" -e container_start_number=21 -e container_end_number=29 -e type=oraclelinux6

start-container-bdporaclelinux7:
	ansible-playbook -i hosts container/playbook.yml --extra-vars="@vars.yml" -e container_start_number=31 -e container_end_number=39 -e type=oraclelinux7

start-container-bdpfedora21:
	ansible-playbook -i hosts container/playbook.yml --extra-vars="@vars.yml" -e container_start_number=41 -e container_end_number=49 -e type=fedora21

stop-all-containers:
	for x in `systemctl |grep bdpcontainer | awk '{print $$5}' | cut -f 1 -d '.'`; do sudo systemctl stop $$x; done;
	sudo systemctl stop weave

destroy-all-containers:
	for x in `docker ps -a |grep bdpcontainer | awk '{print $$12}'`; do sudo docker rm $$x; done;
	sudo /usr/local/bin/weave hide `sudo cat /etc/weave.env | grep ADDR | cut -f 2 -d \"` 


pull-repos:
	docker pull bdpzone/bdpcentos6:latest
	docker pull bdpzone/bdpcentos7:latest
	docker pull bdpzone/bdporaclelinux6:latest
	docker pull bdpzone/bdporaclelinux7:latest
	docker pull bdpzone/bdpfedora21:latest
	docker pull bdpzone/bdpopenvpn:latest

build-repo-server:
	docker build --no-cache=true -t bdpzone/repo-server:latest repo-server

start-server:
	docker run -d --privileged=true -h repo-server --name repo-server -p 2224:22 -p 80:80 -v /opt/mirror:/mirror -v /sys/fs/cgroup:/sys/fs/cgroup:ro bdpzone/repo-server:latest

update-repo-server:
	docker run -ti -v /opt/mirror:/mirror bdpzone/repo-server:latest update_repos
